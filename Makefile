pkgs = 'sml-5.0 spinalDynamics'


LIBRARIES=-ldl `pkg-config` $(shell pkg-config --libs $(pkgs))
CXXFLAGS=-O3 -std=gnu++0x $(shell pkg-config --cflags $(pkgs))  -msse3

CXX_SOURCES = $(wildcard *.cc)


# CXX=g++
DEPS = $(CXX_SOURCES)
OBJ = Rob.o main.o 

%.o: %.c $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(LIBRARIES)

hibso_controller: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBRARIES)


clean:
	rm -f *.o hibso_controller 
