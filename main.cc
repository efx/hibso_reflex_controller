#include <stdio.h>
#include <sml/sml.hh>

#include "Rob.hh"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sml/sml-tools/PhaseManager.hh>
#include "LoggerExtension.hh"

using namespace std;

CentralClock * centralclock;
float debug=false;
std::map<std::string, float_int_string> Settings::sets;


extern EventManager * eventManager;
extern SmlParameters parameters;

void loadControlParametersName(std::map<std::string, std::map<std::string, float> >& param);
void loadGeyerParametersName(std::map<std::string, std::map<std::string, float> >& param);

int main(int argc, char* argv[])
{

  cout << "1) Loading settings...............";

  Settings::config_path = "./conf";


	if(argc >= 2)
		Settings::filename = argv[1];

  if(argc >= 3){
		Settings::prefix = "_";
		Settings::prefix += argv[2];
	}
  //Settings::filename  = "settings_nojw_notf.xml";
  //Settings::prefix  = "_sample";

  cout << Settings::prefix << endl;
  Settings settings;

  Settings::set<string>("config","./conf/");
  Settings::set<string>("save_for_matlab_path","./raw_files/");


  Settings::set<int>("notf_trunklean",1);
	Settings::set<int>("buggyJwang_changes",0);

  srand(time(0));

  cout << "3) Create CentralClock...............";
  centralclock = new CentralClock(parameters[1]["freq_change"]);
  cout << "Ok" << endl;

  cout << "4) Create Robot...............";
  Rob *HibsoController = new Rob();
	cout << "Ok" << endl;


  eventManager->set<bool>(STATES::IS_LAST_PHASE,false);
  PhaseManager::init(HibsoController);

  LoggerManager::loggers["MusculoSkeletalSystem"] = new MusculoSkeletalLogger(*HibsoController);
  LoggerManager::loggers["ReflexSystem"] = new ReflexControllerLogger(HibsoController->geyerController);
  //LoggerManager::loggers["BodySystem"] = new BodyLogger(*HibsoController);
  //LoggerManager::loggers["Perturbation"] = new PerturbationLogger(&(HibsoController->perturbator));
  LoggerManager::init();

  PhaseManager::update();
  HibsoController->step(); // this needs to be shorter than 1ms on the hibso
  int i=0; int maxIndex=9000;
  auto duration = 0.0;
  while(i<9000){
    auto start = std::clock();
    PhaseManager::update();

    static int f=0;
    if(f<1){
        //parameters.printAll();
        //eventManager->printAll();
        Settings::printAll();
        f++;
    }


    //PhaseManager::update();
    HibsoController->step(); // this needs to be shorter than 1ms on the hibso
    //LoggerManager::step(); 

    duration += ( std::clock() - start ) / (float) CLOCKS_PER_SEC;
    //std::cout<<"duration: "<< duration*1000 << " (ms)" <<'\n';

		i++;
	}
  std::cout<<"average: "<< duration*1000/i << " (ms)" <<'\n';

  HibsoController->writeOutputToFile();

	return 0;

}
