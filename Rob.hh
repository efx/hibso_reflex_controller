#ifndef __Rob_HH__
#define __Rob_HH__

#include <string>
#include <math.h>
#include <vector>
#include <map>

//#include <boost/algorithm/string.hpp>

#include <sml/types/types.h>
#include <sml/sml-tools/PerturbationManager.hh>
#include <sml/musculoSkeletalSystem/GeyerSml.hh>

#define DEBUG 0
#define INPUT_SIZE 10000

class Rob: public GeyerSml
{
private:
    int index;

    float inputValueArray[10][INPUT_SIZE];
    float outputValueArray[6][INPUT_SIZE];

    void ConstantInit();
    void InputInit();
    void InputUpdate();
    void loadInputFile();


public:
    typedef GeyerSml sml;
    int step();

    //constructor
    Rob(): GeyerSml(){
        index = 0;
        sml::init();
        // Loading of the inputValueArray
        this->loadInputFile();
    }

    void writeOutputToFile();

};

#endif /* __ROB_HH__ */
