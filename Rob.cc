#include "Rob.hh"
#include <sml/sml-tools/Settings.hh>
#include <boost/xpressive/xpressive.hpp>
#include <string>
#include <math.h>
#include <vector>
#include <fstream>
//std::fstream

#include <map>
#include <string>
#include <iostream>


using namespace std;
using namespace boost::xpressive;

#define ANGLE_HIPLEFT 0
#define ANGLE_KNEELEFT 1
#define ANGLE_ANKLELEFT 2

#define ANGLE_HIPRIGHT 3
#define ANGLE_KNEERIGHT 4
#define ANGLE_ANKLERIGHT 5

#define SENSOR_HEELLEFT 6
#define SENSOR_TOELEFT 7

#define SENSOR_HEELRIGHT 8
#define SENSOR_TOERIGHT 9

#define THETATRUNK 10


extern float debug;
extern EventManager* eventManager;
extern SmlParameters parameters;


void Rob::writeOutputToFile(){
  ofstream outputfile("./data/output_file.txt", ios::out | ios::trunc);

  if(outputfile)
  {
    outputfile << "TORQUE_HIP_LEFT TORQUE_KNEE_LEFT TORQUE_ANKLE_LEFT TORQUE_HIP_RIGHT TORQUE_KNEE_RIGHT TORQUE_ANKLE_RIGHT" << endl;

    for (int i = 0; i <= INPUT_SIZE; i++)
    {
      for(int j = 0; j <= 5; j++)
        outputfile << outputValueArray[j][i] << " ";

      outputfile << endl;
     }

     outputfile.close();
   }
   else
    cerr << "Error ! output_file.txt is open ?" << endl;
}

void Rob::loadInputFile(){

  ifstream joint_angles_file("./data/joints_angle1", ios::in);

  if(joint_angles_file)
  {
    int i = 0; string line1;

    getline(joint_angles_file, line1);
    while(getline(joint_angles_file, line1))
    {
      if(i> INPUT_SIZE) break;
      std::istringstream joint_angles(line1);
      joint_angles >> inputValueArray[ANGLE_HIPLEFT][i];
      joint_angles >> inputValueArray[ANGLE_KNEELEFT][i];
      joint_angles >> inputValueArray[ANGLE_ANKLELEFT][i];
      joint_angles >> inputValueArray[ANGLE_HIPRIGHT][i];
      //cout << inputValueArray[ANGLE_HIPRIGHT][i] << endl;
      //cout << i << ":" << inputValueArray[ANGLE_HIPRIGHT][i] << endl;
      joint_angles >> inputValueArray[ANGLE_KNEERIGHT][i];
      joint_angles >> inputValueArray[ANGLE_ANKLERIGHT][i];
      i++;
    }

    joint_angles_file.close();
  }
  else
    cerr << "Error ! Impossible to open ''joint_angles'' !" << endl;

  ifstream grf_file("./data/grf1", ios::in);

  if(grf_file)
  {
      int i = 0; string line2;
      getline(grf_file, line2);
      while(getline(grf_file, line2))
      {
        if(i> INPUT_SIZE) break;
        std::istringstream iss(line2);
        iss >> inputValueArray[SENSOR_HEELLEFT][i];
        iss >> inputValueArray[SENSOR_HEELRIGHT][i];
        iss >> inputValueArray[SENSOR_TOELEFT][i];
        iss >> inputValueArray[SENSOR_TOERIGHT][i];

        i++;
    }

    grf_file.close();
  }
  else
      cerr << "Error ! Impossible to open ''grf1'' !" << endl;

  ifstream segments_file("./data/segments1", ios::in);

  if(segments_file)
  {
    int i = 0; string line3;
    getline(segments_file, line3);
    while(getline(segments_file, line3))
    {
      if(i> INPUT_SIZE) break;
      std::istringstream segments(line3);

      segments >> inputValueArray[THETATRUNK][i];
      i++;
    }
    segments_file.close();
  }
  else
      cerr << "Error ! Impossible to open ''segments'' !" << endl;

}


void Rob::InputUpdate(){


    sml::Input[INPUT::ANGLE_HIP_LEFT] = inputValueArray[ANGLE_HIPLEFT][index];
    sml::Input[INPUT::ANGLE_KNEE_LEFT] = inputValueArray[ANGLE_KNEELEFT][index];
    sml::Input[INPUT::ANGLE_ANKLE_LEFT] = inputValueArray[ANGLE_ANKLELEFT][index];

    
    sml::Input[INPUT::ANGLE_HIP_RIGHT] = inputValueArray[ANGLE_HIPRIGHT][index];
    sml::Input[INPUT::ANGLE_KNEE_RIGHT] = inputValueArray[ANGLE_KNEERIGHT][index];
    sml::Input[INPUT::ANGLE_ANKLE_RIGHT] = inputValueArray[ANGLE_ANKLERIGHT][index];

    sml::Input[INPUT::SENSOR_HEEL_LEFT] = -inputValueArray[SENSOR_HEELLEFT][index];
    sml::Input[INPUT::SENSOR_HEEL_RIGHT] = -inputValueArray[SENSOR_HEELRIGHT][index];
    sml::Input[INPUT::SENSOR_TOE_LEFT] = -inputValueArray[SENSOR_TOELEFT][index];
    sml::Input[INPUT::SENSOR_TOE_RIGHT] = -inputValueArray[SENSOR_TOERIGHT][index];

    sml::Input[INPUT::THETA_TRUNK] = inputValueArray[THETATRUNK][index];

    //cout << index << ":" << inputValueArray[ANGLE_HIPRIGHT][index] << endl;
}
int Rob::step(){

    sml::step();

    int i=0;

    for (
        unsigned joint=JOINT::FIRST_JOINT,
                 joint_tau=OUTPUT::FIRST_JOINT;
        joint_tau<=OUTPUT::LAST_JOINT;
        joint_tau++,
        joint++
        ){
            outputValueArray[i][index] = sml::Output[joint_tau];
            i++;
    }
    index++;
    return 0;
}


void Rob::ConstantInit(){
    // Segment constant
    sml::Constant[CONSTANT::LENGTH_TRUNK] = 0.8;
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.5;
    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.5;
    sml::Constant[CONSTANT::LENGTH_ANKLE] = 0.1;
    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.16;
    sml::Constant[CONSTANT::WEIGHT_TRUNK] = 53.5;
    sml::Constant[CONSTANT::WEIGHT_THIGH] = 8.5;
    sml::Constant[CONSTANT::WEIGHT_SHIN] = 3.5;
    sml::Constant[CONSTANT::WEIGHT_ANKLE] = 0.0;
    sml::Constant[CONSTANT::WEIGHT_FOOT] = 1.25;
    sml::Constant[CONSTANT::MODEL_WEIGHT] = 70;
    sml::Constant[CONSTANT::MODEL_HEIGHT] = 1.8;

    // Joints constant
    sml::Constant[CONSTANT::REF_POS_HIP_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_HIP_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_HIP_LEFT] = 50.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MAX_POS_HIP_RIGHT] = 50.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIP_LEFT] = -160.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIP_RIGHT] = -160.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];

#ifdef MODEL3D
    sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT] = 0.0;
    sml::Constant[CONSTANT::REF_POS_HIPCOR_RIGHT] = 0.0;
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_LEFT] = 80.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MAX_POS_HIPCOR_RIGHT] = 80.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_LEFT] = 0.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
    sml::Constant[CONSTANT::MIN_POS_HIPCOR_RIGHT] = 0.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIPCOR_LEFT];
#endif



    sml::Constant[CONSTANT::REF_POS_KNEE_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_KNEE_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_KNEE_LEFT] = 135  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_POS_KNEE_RIGHT] = 135  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_KNEE_LEFT] = 5  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_KNEE_RIGHT] = 5  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];



    sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT] = 0;
    sml::Constant[CONSTANT::REF_POS_ANKLE_RIGHT] = 0;
    sml::Constant[CONSTANT::MAX_POS_ANKLE_LEFT] = 40.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_POS_ANKLE_RIGHT] = 40.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_ANKLE_LEFT] = -20.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MIN_POS_ANKLE_RIGHT] = -20.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    // Mono-articular Muscle constant


    sml::Constant[CONSTANT::R_SOL] = 0.05;
    sml::Constant[CONSTANT::R_TA] = 0.04;
    sml::Constant[CONSTANT::R_VAS] = 0.06;
    sml::Constant[CONSTANT::R_GLU] = 0.10;
    sml::Constant[CONSTANT::R_HF] = 0.10;
    sml::Constant[CONSTANT::MAX_PHI_SOL] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_TA] = -10.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_VAS] = 15.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_GLU] = -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::MAX_PHI_HF] = -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_SOL] = -10.0  * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_TA] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_VAS] = 55.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_GLU] = -30.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::REF_PHI_HF] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];;
    sml::Constant[CONSTANT::OPT_L_SOL] = 0.04;
    sml::Constant[CONSTANT::OPT_L_TA] = 0.06;
    sml::Constant[CONSTANT::OPT_L_VAS] = 0.08;
    sml::Constant[CONSTANT::OPT_L_GLU] = 0.11;
    sml::Constant[CONSTANT::OPT_L_HF] = 0.11;
    sml::Constant[CONSTANT::SLACK_L_SOL] = 0.26;
    sml::Constant[CONSTANT::SLACK_L_TA] = 0.24;
    sml::Constant[CONSTANT::SLACK_L_VAS] = 0.23;
    sml::Constant[CONSTANT::SLACK_L_GLU] = 0.13;
    sml::Constant[CONSTANT::SLACK_L_HF] = 0.10;
    sml::Constant[CONSTANT::MAX_V_SOL] = 6.0;
    sml::Constant[CONSTANT::MAX_V_TA] = 12.0;
    sml::Constant[CONSTANT::MAX_V_VAS] = 12.0;
    sml::Constant[CONSTANT::MAX_V_GLU] = 12.0;
    sml::Constant[CONSTANT::MAX_V_HF] = 12.0;
    sml::Constant[CONSTANT::MASS_SOL] = 1.36;
    sml::Constant[CONSTANT::MASS_TA] = 0.29;
    sml::Constant[CONSTANT::MASS_VAS] = 2.91;
    sml::Constant[CONSTANT::MASS_GLU] = 1.4;
    sml::Constant[CONSTANT::MASS_HF] = 1.87;
    sml::Constant[CONSTANT::PENNATION_SOL] = 0.5;
    sml::Constant[CONSTANT::PENNATION_TA] = 0.7;
    sml::Constant[CONSTANT::PENNATION_VAS] = 0.7;
    sml::Constant[CONSTANT::PENNATION_GLU] = 0.5;
    sml::Constant[CONSTANT::PENNATION_HF] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_SOL] = 0.81;
    sml::Constant[CONSTANT::TYPE1FIBER_TA] = 0.7;
    sml::Constant[CONSTANT::TYPE1FIBER_VAS] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_GLU] = 0.5;
    sml::Constant[CONSTANT::TYPE1FIBER_HF] = 0.5;
    sml::Constant[CONSTANT::DIRECTION_SOL] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_TA] = 1.0;
    sml::Constant[CONSTANT::DIRECTION_VAS] = 1.0;
    sml::Constant[CONSTANT::DIRECTION_GLU] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_HF] = 1.0;
    // Mono-articular Muscle constant
    sml::Constant[CONSTANT::R_GAS_ANKLE] = 0.05;
    sml::Constant[CONSTANT::R_GAS_KNEE] = 0.05;
    sml::Constant[CONSTANT::R_HAM_KNEE] = 0.05;
    sml::Constant[CONSTANT::R_HAM_HIP] = 0.08;
    sml::Constant[CONSTANT::MAX_PHI_GAS_ANKLE] = 20.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_GAS_KNEE] = 40.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_HAM_KNEE] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::MAX_PHI_HAM_HIP] =  -180.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::REF_PHI_GAS_ANKLE] = -10.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_ANKLE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_GAS_KNEE] = 15.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_HAM_KNEE] = 0.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_KNEE_LEFT];
    sml::Constant[CONSTANT::REF_PHI_HAM_HIP] = -25.0 * (2.0*PI/360.0)+sml::Constant[CONSTANT::REF_POS_HIP_LEFT];
    sml::Constant[CONSTANT::OPT_L_GAS] = 0.05;
    sml::Constant[CONSTANT::OPT_L_HAM] = 0.10;
    sml::Constant[CONSTANT::SLACK_L_GAS] = 0.40;
    sml::Constant[CONSTANT::SLACK_L_HAM] = 0.31;
    sml::Constant[CONSTANT::MAX_V_GAS] = 12.0;
    sml::Constant[CONSTANT::MAX_V_HAM] = 12.0;
    sml::Constant[CONSTANT::MASS_GAS] = 0.45;
    sml::Constant[CONSTANT::MASS_HAM] = 1.82;
    sml::Constant[CONSTANT::PENNATION_GAS] = 0.7;
    sml::Constant[CONSTANT::PENNATION_HAM] = 0.7;
    sml::Constant[CONSTANT::TYPE1FIBER_GAS] = 0.54;
    sml::Constant[CONSTANT::TYPE1FIBER_HAM] = 0.44;
    sml::Constant[CONSTANT::DIRECTION_GAS] = -1.0;
    sml::Constant[CONSTANT::DIRECTION_HAM] = -1.0;

   if(Settings::get<int>("coman")){
    sml::Constant[CONSTANT::LENGTH_TRUNK] = 0.8;
    sml::Constant[CONSTANT::LENGTH_THIGH] = 0.5;
    sml::Constant[CONSTANT::LENGTH_SHIN] = 0.5;
    sml::Constant[CONSTANT::LENGTH_ANKLE] = 0.1;
    sml::Constant[CONSTANT::LENGTH_FOOT] = 0.16;
    sml::Constant[CONSTANT::WEIGHT_TRUNK] = 53.5;
    sml::Constant[CONSTANT::WEIGHT_THIGH] = 8.5;
    sml::Constant[CONSTANT::WEIGHT_SHIN] = 3.5;
    sml::Constant[CONSTANT::WEIGHT_ANKLE] = 0.0;
    sml::Constant[CONSTANT::WEIGHT_FOOT] = 1.25;
    sml::Constant[CONSTANT::MODEL_WEIGHT] = 70;
    sml::Constant[CONSTANT::MODEL_HEIGHT] = 0.9;
    }
}


void Rob::InputInit(){

  sml::Input[INPUT::NECK_Y]       = 0.0;
  sml::Input[INPUT::NECK_X]       = 0.0;
	sml::Input[INPUT::TOE_LEFT_X]   = 0.0;
	sml::Input[INPUT::TOE_LEFT_Y]   = 0.0;
	sml::Input[INPUT::TOE_LEFT_Z]   = 0.0;
	sml::Input[INPUT::TOE_RIGHT_X]  = 0.0;
	sml::Input[INPUT::TOE_RIGHT_Y]  = 0.0;
	sml::Input[INPUT::TOE_RIGHT_Z]  = 0.0;
	sml::Input[INPUT::HEEL_LEFT_X]  = 0.0;
	sml::Input[INPUT::HEEL_LEFT_Y]  = 0.0;
	sml::Input[INPUT::HEEL_LEFT_Z]  = 0.0;
	sml::Input[INPUT::HEEL_RIGHT_X] = 0.0;
	sml::Input[INPUT::HEEL_RIGHT_Y] = 0.0;
	sml::Input[INPUT::HEEL_RIGHT_Z] = 0.0;

	sml::Input[INPUT::MIDHIP_X] = 0.0;
	sml::Input[INPUT::MIDHIP_Y] = 0.0;
	sml::Input[INPUT::MIDHIP_Z] = 0.0;

  InputUpdate();
}

