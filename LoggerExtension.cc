#include "LoggerExtension.hh"

using namespace std;
extern EventManager * eventManager;
extern bool debug;


void BodyLogger::writeHeader(){
	*raw_files["segments"] << "trunk_angle" << " "
                           << "hip_left_x" << " "
                           << "hip_left_y" << " "
                           << "hip_left_y" << " "
                           << "knee_left_x" << " "
                           << "knee_left_y" << " "
                           << "knee_left_y" << " "
                           << "ankle_left_x" << " "
                           << "ankle_left_y" << " "
                           << "ankle_left_y" << " "
                           << "toe_left_x" << " "
                           << "toe_left_y" << " "
                           << "toe_left_y" << " "
                           << "heel_left_x" << " "
                           << "heel_left_y" << " "
                           << "heel_left_y" << " "
                           << "hip_right_x" << " "
                           << "hip_right_y" << " "
                           << "hip_right_y" << " "
                           << "knee_right_x" << " "
                           << "knee_right_y" << " "
                           << "knee_right_y" << " "
                           << "ankle_right_x" << " "
                           << "ankle_right_y" << " "
                           << "ankle_right_y" << " "
                           << "toe_right_x" << " "
                           << "toe_right_y" << " "
                           << "toe_right_y" << " "
                           << "heel_right_x" << " "
                           << "heel_right_y" << " "
                           << "heel_right_y" << " "
                           ;
	
	*raw_files["distance"] << "distance";
	*raw_files["footfall"] << "left right";
	*raw_files["energy"] << "energy";
	*raw_files["grf"] << "SENSOR_HEEL_LEFT SENSOR_HEEL_RIGHT SENSOR_TOE_LEFT SENSOR_TOE_RIGHT";
	for(auto &kv : raw_files)
		*kv.second << endl;
}

void BodyLogger::writeContent(){
	*raw_files["segments"] << rob.Input[INPUT::THETA_TRUNK] << " "
                           << rob.supervisor::getFromDef("LEFT_THIGH")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("LEFT_THIGH")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("LEFT_THIGH")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("LEFT_SHIN")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("LEFT_SHIN")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("LEFT_SHIN")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("LEFT_FOOT")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("LEFT_FOOT")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("LEFT_FOOT")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("SENSOR_TOE_LEFT")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[0]<< " "
                           << rob.supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("SENSOR_HEEL_LEFT")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("RIGHT_THIGH")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("RIGHT_THIGH")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("RIGHT_THIGH")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("RIGHT_SHIN")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("RIGHT_SHIN")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("RIGHT_SHIN")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("RIGHT_FOOT")->getPosition()[0] << " "
                           << rob.supervisor::getFromDef("RIGHT_FOOT")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("RIGHT_FOOT")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[0]<< " "
                           << rob.supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("SENSOR_TOE_RIGHT")->getPosition()[2] << " "
                           << rob.supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[0]<< " "
                           << rob.supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[1] << " "
                           << rob.supervisor::getFromDef("SENSOR_HEEL_RIGHT")->getPosition()[2] << " " 
                           << endl;
	print_debug("--writing segments");

	*raw_files["distance"] << eventManager->get<float>(STATES::DISTANCE);
	*raw_files["footfall"] << rob.left_foot->inStance() << " " <<rob.right_foot->inStance();
	*raw_files["energy"] << eventManager->get<float>(STATES::ENERGY);
	*raw_files["grf"] << 
		rob.getTouchSensor(INPUT::toString(INPUT::SENSOR_HEEL_LEFT))->getValues()[2] 
		<< " " <<
		rob.getTouchSensor(INPUT::toString(INPUT::SENSOR_HEEL_RIGHT))->getValues()[2] 
      << " " <<
      rob.getTouchSensor(INPUT::toString(INPUT::SENSOR_TOE_LEFT))->getValues()[2] 
      << " " <<
      rob.getTouchSensor(INPUT::toString(INPUT::SENSOR_TOE_RIGHT))->getValues()[2];

	for(auto &kv : raw_files)
		*kv.second << endl;
}

void PerturbationLogger::writeHeader(){
   cout << "perturbationLogger writeHeader: " << perturbator->getHeader() << endl;
   cout << "perturbationLogger writeHeader: " << perturbator->getHeader() << endl;
	//*raw_files["perturbation"] << "hello" << endl;
   *raw_files["perturbation"] << perturbator->getHeader();
	for(auto &kv : raw_files) 
		*kv.second << endl;
}

void PerturbationLogger::writeContent(){
	*raw_files["perturbation"] << perturbator->getRow();
	for(auto &kv : raw_files)
	  *kv.second << endl;
}

